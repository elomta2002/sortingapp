import java.util.Arrays;
import java.util.Scanner;

/**
 * app takes up to 10 integer from console and  sorts them
 * @author elomta2002
 */
public class SortingApp {
    /**
     * method that simpy prints 1d array  with elements that should be sorted
     * @param arr array that should be sorted
     */
    private static void printArr(int[] arr) {
        StringBuilder sb=new StringBuilder();
        if (arr.length==0)
            return;
        sb.append(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            sb.append(" "+arr[i]);
        }
        String res=sb.toString();
        System.out.println(res);
    }

    /**
     * main
     * it trows illegalArgumentException if input does not correspond standards from task description
     * @param Args
     */
    public  static void main(String[] Args){
        Scanner sc=new Scanner(System.in);
        String in = sc.nextLine().trim();
        if (in.equals(" ")){
            throw new IllegalArgumentException("there is no input");
        }
        String[] inputs = in.split(" ", 11);
        if (inputs.length>10){
            throw new IllegalArgumentException("too many arguments works for up to 10");
        }
        int[] input = new int[inputs.length];
        try {
            for (int i = 0; i < input.length; i++) {
                    input[i] = Integer.parseInt(inputs[i]);
                }
            Arrays.sort(input);
            printArr(input);
        }
        catch (NumberFormatException e) {
           throw new IllegalArgumentException("you could only input integers");
        }
    }

}
