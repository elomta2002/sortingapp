import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
/**
 * test if Sorting app correctly sorts
 * parametrized tests
 * @author elomta2002
 */
@RunWith(Parameterized.class)
public class testSortingApp {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"1 2 3 4 5 7","1 2 3 4 5 7"},{"3","3"},{"1 2 5 8 3 6","1 2 3 5 6 8"},
                {"1 3 2 5 4 9 8 7 6 10","1 2 3 4 5 6 7 8 9 10"},{" 134 5 576 998 43","5 43 134 576 998"}
        });
    }

    /**
     *
     * @param inputText   input array as a string
     * @param expected    expected output array as a string
     */
    public testSortingApp(String inputText, String expected) {
        this.inputText = inputText;
        this.expected = expected;
    }

    private String inputText;
    private String expected;

    /**
     * unit test
     */
   @Test
    public void test() {

        final ByteArrayInputStream controlledIn = new ByteArrayInputStream(inputText.getBytes(StandardCharsets.UTF_8));
        InputStream defaultIn = System.in;

        final ByteArrayOutputStream sink = new ByteArrayOutputStream();
        PrintStream controlledOut = new PrintStream(sink);
        PrintStream defaultOut = System.out;

        try {
            System.setIn(controlledIn);
            System.setOut(controlledOut);

            SortingApp.main(new String[]{});

            controlledOut.flush();
            assertEquals(expected, sink.toString().trim());
        } finally {
            System.setIn(defaultIn);
            System.setOut(defaultOut);
        }
    }
}
