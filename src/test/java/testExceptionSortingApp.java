import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
/**
 * test if SortingApp trows IllegalArgumentException when it should.
 * parametrized tests
 * @author elomta2002
 */
@RunWith(Parameterized.class)
public class testExceptionSortingApp {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
               {" "},{"1 5 2 1 1 2 3 4 5 65 6"},{"2 4 5 1 3fd"},{"df"},{"2 4 5 1 df3 5 6"},{"2 4 5 1 5.3 5 6"}
        });
    }

    /**
     *
     * @param inputText  input integers
     */
    public testExceptionSortingApp(String inputText) {
        this.inputText = inputText;
    }
    private String inputText;
    /**
     * unit test
     */
    @Test(expected = IllegalArgumentException.class)
    public void test() {

        final ByteArrayInputStream controlledIn = new ByteArrayInputStream(inputText.getBytes(StandardCharsets.UTF_8));
        InputStream defaultIn = System.in;
        try {
            System.setIn(controlledIn);
            SortingApp.main(new String[]{});
        } finally {
            System.setIn(defaultIn);
        }
    }
}
